package pege_collecter_pc.test;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import library.java.SQLiteAbstractDaoClass.DATACLASS;


public class DirectoryUrlDaoClass{
	private Statement stmt;
	private ResultSet rs;
	private Connection con;
	
	//edit 
	private static final String DATABASE_NAME = "urls";
	private String TABLE_NAME = "url";
	public static final ColumnClass[] COLUMNS = {
			
			new ColumnClass("id", "int"),
			new ColumnClass("topPage", "text"),
			new ColumnClass("directory", "text"),
			new ColumnClass("haveLoaded", "int")

			};
	private static final String WORKSPACE = "";
	
	public DirectoryUrlDaoClass() {
		
		// JDBCドライバーの指定
		try {
			Class.forName("org.sqlite.JDBC");

			// データベースに接続する なければ作成される
			
			con = DriverManager.getConnection("jdbc:sqlite:" + WORKSPACE
					+ DATABASE_NAME + ".db");
			stmt = con.createStatement();
			
			rs = stmt.executeQuery("select count(*) from sqlite_master where type='table' and name='"
							+ TABLE_NAME + "';");

			// テーブル作成
			if (rs.getInt(1) == 0) {

				String sql = "create table " + TABLE_NAME + "( ";

				sql = sql + COLUMNS[0].name + " " + COLUMNS[0].type;
				for (int i = 1; i < COLUMNS.length; i++) {
					sql = sql + ", " + COLUMNS[i].name + " " + COLUMNS[i].type;
				}
				sql = sql + ", PRIMARY KEY (`id`))";

				stmt.executeUpdate(sql);

			}
		} catch (ClassNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}
	
	
	public void insert(DirectoryUrlDataClass entity){
		//edit
		String sql = getInsertSqlFormatForPrimary();
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			setOneData(ps, entity);
			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	/**
	 * this for primary key
	 * @return
	 */
	private String getInsertSqlFormatForPrimary(){
		String sql = "insert into " + TABLE_NAME + " (";
		//has primary key
		sql += "'" + COLUMNS[1].name + "'";
		for(int i = 2; i < COLUMNS.length; i++) {
			sql += ", " + "'" + COLUMNS[i].name + "'";
		}

		sql += ") VALUES (?";
		for(int i = 2; i < COLUMNS.length; i++) {
			sql += ",?";
		}
		sql += ")";
		return sql;
	}
	private String getInsertSqlFormat(){
		//don't have primary key
		String sql = "insert into " + TABLE_NAME + " (";
		sql += "'" + COLUMNS[0].name + "'";
		for(int i = 1; i < COLUMNS.length; i++) {
			sql += ", " + "'" + COLUMNS[i].name + "'";
		}
		sql += ") VALUES (?";
		for(int i = 1; i < COLUMNS.length; i++) {
			sql += ",?";
		}
		sql += ")";
		return sql;
	}
	private PreparedStatement setOneData(PreparedStatement ps, DirectoryUrlDataClass entity) throws SQLException{
		//edit
		ps.setString(1, entity.topPage);
		ps.setString(2, entity.directory);
		ps.setBoolean(3, entity.haveLoaded);
		return ps;
	}
	
	public void insert(List<DirectoryUrlDataClass> entityList){
		try {
			//edit
			String sql = getInsertSqlFormatForPrimary();

			con.setAutoCommit(false);
			PreparedStatement ps = con.prepareStatement(sql);
			for(int i=0; i < entityList.size(); i++){
				DirectoryUrlDataClass entity = entityList.get(i);
				setOneData(ps, entity);
				ps.executeUpdate();

			}
			con.commit();
            ps.close();
            con.setAutoCommit(true);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}


	public List<DirectoryUrlDataClass> selectAll() {
		List<DirectoryUrlDataClass> entityList = new ArrayList<DirectoryUrlDataClass>();
		 PreparedStatement ps;
		// 結果を表示する
		try {
			rs = stmt.executeQuery("select * from " + TABLE_NAME);
			while (rs.next()) {

				entityList.add(toEntity(rs) );

			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return entityList;
	}

	private DirectoryUrlDataClass toEntity(ResultSet rs) throws SQLException {

		DirectoryUrlDataClass entity = new DirectoryUrlDataClass();
		//edit
		entity.id = rs.getInt(COLUMNS[0].name);
		entity.topPage = rs.getString(COLUMNS[1].name);
		entity.directory = rs.getString(COLUMNS[2].name);
		entity.haveLoaded = rs.getBoolean(COLUMNS[3].name);
		



		return entity;

	}

	
	
	

	public static class ColumnClass{
		public String name;
		public String type;
		
		public ColumnClass(String name, String type){
			this.name = name;
			this.type = type;
		}
	}
	////////////////////////////////////////not updated//////////////////////////////////////////////
	
	
	public boolean existsSame(DirectoryUrlDataClass entity){
		String sql = "select count() from " + TABLE_NAME + " where "
				+ "topPage = '" + entity.topPage + "' and "
				+ "directory = '" + entity.directory + "'";
		
		try {
			rs = stmt.executeQuery(sql);
			if(1 <= rs.getInt(1)){
				return true;
			}
			else{
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
	}
	
	public boolean insertIfOriginal(DirectoryUrlDataClass entity){
		if(existsSame(entity)){
			return false;
		}
		else{
			
			insert(entity);
			return true;
		}
	}
	/**
	 * initialize whether this page is downloaded or not;
	 */
	public  void initialize(){
		List<DirectoryUrlDataClass> entitys = selectAll();
		for (DirectoryUrlDataClass entity : entitys) {
			entity.haveLoaded = false;
		}
		
		insert(entitys);
		
	}
	
	public String[] getMainPage(){
		List<DirectoryUrlDataClass> entitys = selectWhereAll("directory =''");
		List<String> page = new ArrayList<String>();
		for (DirectoryUrlDataClass entity : entitys) {
			page.add(entity.topPage);
		}
		return page.toArray(new String[0]);
	}
	
	public List<DirectoryUrlDataClass> selectWhereAll(String conditions){
		List<DirectoryUrlDataClass> entityList = new ArrayList<DirectoryUrlDataClass>();
		 PreparedStatement ps;
		// 結果を表示する
		try {
			rs = stmt.executeQuery("select * from " + TABLE_NAME + " where " + conditions);
			while (rs.next()) {

				entityList.add(toEntity(rs) );

			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return entityList;
	}
	
	public DirectoryUrlDataClass selectWhere(String conditions){
		DirectoryUrlDataClass entity = null;
		 PreparedStatement ps;
		// 結果を表示する
		try {
			rs = stmt.executeQuery("select * from " + TABLE_NAME + " where " + conditions);
			if(rs.next()){
				entity = toEntity(rs);
			}
			else{
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return entity;
	}
	
	
	public DirectoryUrlDataClass getOneUrlNotLoaded(String topPage){
		return selectWhere("topPage ='" + topPage +"' and haveLoaded = 0");
	}
	
	
}
