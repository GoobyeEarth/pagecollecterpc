package pege_collecter_pc.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import library.java.LibJavaClass;

public class UsingMethodTestClass {
	public void jsoupAttri(){
		Document document = null;
		try {
			document = Jsoup.connect("http://jp.reuters.com/investing/currencies").get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Elements elements = document.select("#topStory > div.module > div.moduleBody > div.topStory > h2 > a");
		String result = "";
		for (Element element : elements) {
			String tagName = element.tagName();
			String innerHtml = elements.outerHtml();
			result += innerHtml + "\n";
			String attr = element.attr("href");
			System.out.println(attr);
		}
		System.out.println(result);
		
	}
	
	public String[] getUrlOnPage(String pageUrl, String baseUrl){
		List<String> urls = new ArrayList<String>();
		
		Document document = null;
		try {
			document = Jsoup.connect(pageUrl).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Elements elements = document.getElementsByTag("a");
		String result = "";
		for (Element element : elements) {
			
			String attr = element.attr("href");
			
			if(attr.startsWith("http") && attr.startsWith(baseUrl)){
				urls.add(attr);
			}
			
			if(!attr.startsWith("http")){
				attr = baseUrl + attr;
				urls.add(attr);
				
			}
			
		}
		
		return urls.toArray(new String[0]);
	}
	
	private static final int methodOnLogidDesignClass = 0;
//////////////////////////////////////below method is in LogicDesignClass/////////////////////////////////////////
	
	public void insertUrlsIfUnegisteredFromPage(DirectoryUrlDataClass data){
		List<String> hrefAttrs = getHrefAttrElementFromPage(data.topPage, data.directory);
		List<DirectoryUrlDataClass>  excludedUrls = getExcludeExternalUrl(data.topPage, hrefAttrs);
		DirectoryUrlDaoClass urlDao = new DirectoryUrlDaoClass();
		for (DirectoryUrlDataClass excludedUrl : excludedUrls) {
			urlDao.insertIfOriginal(excludedUrl);
		}
		
	}
	
	/**
	 * this is select and get absolute html address
	 */
	public List<DirectoryUrlDataClass> getExcludeExternalUrl(String topPage, List<String> hrefAttrs){
		List<DirectoryUrlDataClass> excludedUrls = new ArrayList<DirectoryUrlDataClass>();
		for (String attr : hrefAttrs) {
			//Overlapping Avoid
			if(attr.endsWith("/")){
				attr = attr.replaceAll("\\/$", "");
			}
			// include #
			if(attr.indexOf("#") != -1){
				attr = attr.replaceFirst("#.*", "");
			}
			
			//http://top.page + /dir
			if(attr.startsWith(topPage)){
				DirectoryUrlDataClass data = new DirectoryUrlDataClass();
				data.directory = attr.replace(topPage, "");
				data.topPage = topPage;
				excludedUrls.add(data);
			}
			//http://not.top.page + /dir
			else if(attr.startsWith("http") && !attr.startsWith(topPage)){
				//ignored
			}
			// /dir
			else if(attr.startsWith("/")){
				DirectoryUrlDataClass data = new DirectoryUrlDataClass();
				data.directory = attr;
				data.topPage = topPage;
				excludedUrls.add(data);
			}
			// topPage
			else if(attr == ""){
				//ignored
			}
			
			else{
				System.out.println("error on ExcludeExternalUrl");
				System.out.println("there is not expected hrefAttrs:" + attr);
			}
		}
		
		return excludedUrls;
	}
	public List<String> getHrefAttrElementFromPage(String topPage, String directory){
		List<String> urls = new ArrayList<String>();
		
		Document document = null;
		try {
			document = Jsoup.connect(topPage + directory).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Elements elements = document.getElementsByTag("a");
		String result = "";
		for (Element element : elements) {
			
			String attr = element.attr("href");
			urls.add(attr);
		}
		
		return urls;
		
	}
	
	public void downloadAsHtml(String topPage, String directory){
		String html = null;
		try {
			Document document = Jsoup.connect(topPage + directory).get();
			Elements elements = document.getElementsByAttribute("href");
			for (Element element : elements) {
				String attr = element.attr("href");
				attr = convertAttrToSavePoint(attr, topPage);
				
				System.out.println(attr);
				element.attr("href", attr);
				
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
	}
	
	public String convertAttrToSavePoint(String attr, String topPage){
		String WORKSPACE ="";
		String DATE = "";
		String TITLE ="";
		String suffix = "";
		
		//Overlapping Avoid
		if(attr.endsWith("/")){
			attr = attr.replaceAll("\\/$", "");
		}
		if(attr.indexOf("#") != -1){
			attr = attr.replaceFirst("#.*", "");
			LibJavaClass.TextData data = LibJavaClass.divide(attr, "#.*");
		}
		
		//http://top.page + /dir
		if(attr.startsWith(topPage)){
			attr = attr.replaceFirst(topPage, "");
		}
		//http://not.top.page + /dir
		else if(attr.startsWith("http") && !attr.startsWith(topPage)){
			
		}
		// /dir
		else if(attr.startsWith("/")){
		}
		// topPage
		else if(attr == ""){
		}
		
		else{
			System.out.println("error on convertAttrToSavePoint#1");
			System.out.println("there is not expected attr:" + attr);
			attr = "errorplace";
		}
		
		if(attr == ""){
			attr = "index.html";
		}
		//html file
		else if(attr.endsWith(".htm") || attr.endsWith(".html") || attr.endsWith(".php")){
			attr = attr.replaceFirst(".php$", ".html");
		}
		else if( attr.indexOf(".") == -1){
			attr += ".html";
		}
		//otherDownloadfile
		else if(attr.endsWith(".pdf") ){
			
		}
		//not DownLoadFile
		else if(false){
			
		}
		else{
			System.out.println("error on convertAttrToSavePoint#2");
			System.out.println("there is not expected attr:" + attr);
			attr = "errorplace#2";
		}
		
		return attr;
	}
	
	
	
	
	
	
	
}
