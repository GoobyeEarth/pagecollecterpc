package pege_collecter_pc.test;

import java.util.ArrayList;
import java.util.List;

import library.java.LibJavaClass;

public class LogicDesignClass {
	/**
	 * problems
	 * when loading site, load as php, or html. or dir/
	 * but saving file we nead to change .php .dir to html
	 * 
	 */
	public void mainDesign(){
		//if do
		initializeDirectorySql();
		String [] sites = selectMainPage();
		
		for (String site : sites) {
			
			saveUrl(site);// to sqlite
			String[] dirs = selectAllDirFromPage(site);
			for (String dir : dirs) {
				savingFoundUrl(site, dir);//to workspace
			}
		}
		
	}
	@Done
	public void initializeDirectorySql(){
		/*
		 * 
		 */
	}
	
	
	
	public boolean isDirOnly(String dir){
		if(dir.indexOf(".") == -1){
			if(dir.endsWith("/")){
				dir = dir.substring(0, dir.length() - 1);
			}
			String savedPlace = "";
			savedPlace += ".html";
			return true;
		}
		else{
			return false;
		}
	}
	@Done
	/**
	 * to sqlite as if has not inserted
	 * @param topPage
	 */
	public void saveUrl(String topPage){
		
//		insert(topPage);
		DirectoryUrlDataClass url;
		
		do {
			url = selectOneUrlNotUsed();
			
			insertUrlsIfUnegisteredFromPage(url);
			
			//insert(scrapeingUrl) to haveLoaded = true;
			
		} while (url != null);
		
		
	}
	
	@Done
	private void insertUrlsIfUnegisteredFromPage(DirectoryUrlDataClass url) {
		// TODO Auto-generated method stub
		
	}
	public void save(){
		
	}
	
	@Deprecated
	private void insertUrlsIfUnegisteredFromPage2(DirectoryUrlDataClass data, List<DirectoryUrlDataClass> allUrlInOneSiteLoadedBeforeHand) {
		
		List<String> hrefAttrs = getHrefAttrElementFromPage(data.topPage, data.directory);
		List<DirectoryUrlDataClass>  excludedUrls = getExcludeExternalUrl(data.topPage, hrefAttrs);
		DirectoryUrlDaoClass urlDao = new DirectoryUrlDaoClass();
		for (DirectoryUrlDataClass excludedUrl : excludedUrls) {
			if(isOriginal(allUrlInOneSiteLoadedBeforeHand, excludedUrl)){
				allUrlInOneSiteLoadedBeforeHand.add(excludedUrl);
			}
		}
		
		/*
		 * if use this method you need to save after all page in one site scraped.
		 * 
		 */
		
	}
	
	public boolean isOriginal(List<DirectoryUrlDataClass> allUrlInOneSiteLoadedBeforeHand, DirectoryUrlDataClass excludedUrl){
		for (DirectoryUrlDataClass dataSaved : allUrlInOneSiteLoadedBeforeHand) {
			if(excludedUrl.directory.equals(dataSaved.directory)){
				return false;
			}
			
			
		}
		return true;
	}
	
	private List<DirectoryUrlDataClass> getExcludeExternalUrl(String topPage, List<String> hrefAttrs) {
		// TODO Auto-generated method stub
		return null;
	}
	private List<String> getHrefAttrElementFromPage(String topPage, String directory) {
		// TODO Auto-generated method stub
		return null;
	}
	public void saveHtmlWithReplace(String topPage, String url){
		
		String savePlace = "";
		
		download(url, savePlace);
		
		String html = loadHtml(savePlace);
		//we need 
//		html = html.replaceAll(topPage, "");
		html = LibJavaClass.replaceAll(html, topPage + "[^\"]*", new LibJavaClass.GetStringSetStringInterface() {
			
			@Override
			public String setProcess(String dir) {
				String urlFromHtmlFile = saveFormatChanger(topPage, dir);
				
				return urlFromHtmlFile;
			}
		});
		
		writefile(savePlace, html);
		
	}
	/**
	 * @see 
	 * @param topPage
	 * @param dir
	 * @return
	 */
	public String saveFormatChanger(String topPage, String dir){
		String savePlace = "";
		//not changed
		if(dir.endsWith(".html") || dir.endsWith(".htm") ){
			savePlace += dir;
			
		}
		//changing webpage to html
		else if (dir.endsWith(".php")){
			dir.replace(".php", ".html");
			savePlace += dir;
			
		}
		else if (dir.indexOf(".") == -1){
			if(dir.endsWith("/")){
				dir.replace("/$", "");
				
			}
			savePlace += dir;
			
		}
		//don't download this
		//picture
		else if(dir.endsWith(".jpg") || dir.endsWith(".jpg")){
			
		}
		
		else{
			System.out.println("there is unrecognized format:"  + dir);
		}
		
		return savePlace;
	}
	@Doing
	public void savingFoundUrl(String topPage, String dir){
		String savePlace = "";
		//not changed
		if(dir.endsWith(".html") || dir.endsWith(".htm") ){
			savePlace += dir;
			
		}
		//changing webpage to html
		else if (dir.endsWith(".php")){
			dir.replace(".php", ".html");
			savePlace += dir;
			download(topPage + dir, savePlace);
		}
		else if (dir.indexOf(".") == -1){
			if(dir.endsWith("/")){
				dir.replace("/$", "");
				
			}
			savePlace += dir;
			download(topPage + dir, savePlace);
		}
		//don't download this
		//picture
		else if(dir.endsWith(".jpg") || dir.endsWith(".jpg")){
			
		}
		
		else{
			System.out.println("there is unrecognized format:"  + dir);
			
		}
	}
	public void savePHPasHtml(String topPage, String url){
		String savePlace =" ";
		
		
	}
	
	private void writefile(String savePlace, String html) {
		// TODO Auto-generated method stub
		
	}

	@Done
	public String[] selectAllDirFromPage(String topPage){
		return null;
	}
	@Done
	public String[] selectMainPage(){
		return null;
	}
	
	public void load(String url){
		
	}
	@Done
	public DirectoryUrlDataClass selectOneUrlNotUsed(){
		return null;
		
	}
	
	public boolean isOriginal(String url){
		return false;
	}
	
	
	
	public void insert(DirectoryUrlDataClass url){
		
	}
	
	public void insert (String[] urls){
		
	}
	
	public void insert(List<String> urls){
		
	}
	
	public void download(String url, String savePlace){
		
		
	}
	
	public void downloadHtml(String url, String savePlace){
		String htmlDounloaded= "";
		
		
	}
	
	public String loadHtml(String savePlace){
		return null;
	}
	
	public void saveHtml(String savePlace, String html){
		
	}
	
	
}
