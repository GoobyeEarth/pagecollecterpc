package pege_collecter_pc.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.eclipse.jgit.api.CheckoutCommand;
import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoFilepatternException;
import org.eclipse.jgit.api.errors.RefAlreadyExistsException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import library.java.FileClass;
import library.java.LibJavaClass;
import pege_collecter_pc.main.KeyInterface;

public class FundamentalFunctionTestClass implements KeyInterface {
	public FundamentalFunctionTestClass() {
		// TODO Auto-generated constructor stub
	}
	
	public void jsoup(){
		Document document = null;
		try {
			document = Jsoup.connect("https://www.google.com/").get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Elements elements = document.select("#fll > a:nth-child(4)");
		String result = "";
		for (Element element : elements) {
			String tagName = element.tagName();
			String innerHtml = elements.outerHtml();
			result += innerHtml + "\n";
		}
		System.out.println(result);
	}
	
	public void usingWorkSpace(){
		FileClass.save(new String[]{"test"}, workspace + "test.txt");
	}
	
	public void downloadhtml(){
		
		try {
			URL url = new URL("http://jp.reuters.com/investing/currencies"); // ダウンロードする URL
			URLConnection conn = url.openConnection();
			InputStream in = conn.getInputStream();
			File file = new File(workspace + "currencies.html"); // 保存先
			FileOutputStream out = new FileOutputStream(file, false);
			byte[] bytes = new byte[512];
			while(true){
			    int ret = in.read(bytes);
			    if(ret <= 0) break;
			    System.out.println("bytes.size:" + bytes.length + " ret:" + ret);
			    out.write(bytes, 0, ret);
			    
			}

			out.close();
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	
	public void jsoupAttri(){
		Document document = null;
		try {
			document = Jsoup.connect("http://jp.reuters.com/investing/currencies").get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Elements elements = document.select("#topStory > div.module > div.moduleBody > div.topStory > h2 > a");
		String result = "";
		for (Element element : elements) {
			String tagName = element.tagName();
			String innerHtml = elements.outerHtml();
			result += innerHtml + "\n";
			String attr = element.attr("href");
			System.out.println(attr);
		}
		System.out.println(result);
		
	}
	
	public void urlDaoInsert(){
		DirectoryUrlDaoClass dao = new DirectoryUrlDaoClass();
		DirectoryUrlDataClass data = new DirectoryUrlDataClass();
		data.directory = "";
		
		data.topPage  = "http://jp.reuters.com";
		dao.insertIfOriginal(data);
		
	}
	
	public void urlDaoSelect(){
		DirectoryUrlDaoClass dao = new DirectoryUrlDaoClass();
		List<DirectoryUrlDataClass> data = dao.selectAll();
		for (DirectoryUrlDataClass url : data) {
			System.out.println("dir:" + url.directory + " url:" + url.topPage);
			
		}
	}
	
	public void gitSample(){
		 FileRepositoryBuilder builder = new FileRepositoryBuilder();
		 Git git = null;
	        try {
				Repository repository = builder.setGitDir(new File("/Users/aberintaro/Documents/workspace/PageCollercter/" + Constants.DOT_GIT)).readEnvironment().findGitDir().build();
				 git = new Git(repository);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
	       
	        // ブランチの切り替え(リモートにあるやつ)
	        try {
	        	
	            // 初めてリモートリポジトリを checkout する場合
	        	CheckoutCommand checkout = git.checkout().setCreateBranch(true).setName("branch ame");
	            checkout.setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.SET_UPSTREAM);
	            checkout.setStartPoint("リポジトリ名/" + "ブランチ名").call(); //# origin/ブランチ名 みたいに設定する

	        } catch (RefAlreadyExistsException e) {
	            // 2回目以降(checkout 済みだと上記例外が投げられるっぽいので)
	            try {
	                git.checkout().setName("ブランチ名").call();
	                git.pull().call();
	            } catch (GitAPIException e1) {
	                throw new RuntimeException(e);
	            }
	        } catch (GitAPIException e) {
	            throw new RuntimeException(e);
	        }

	}

	public void gitSample2(){
		// 新しくリポジトリを作成する。存在するパスを指定すること
		try {
			Repository repo = FileRepositoryBuilder.create(new File("/Users/aberintaro/Documents/workspace/PageCollercter/.git"));
			if( ! repo.getRepositoryState().canCommit()){
				System.out.println("invalide repo");
			}
			File myfile = new File(repo.getDirectory().getParent(), "testfile.txt");
            myfile.createNewFile();
            
            Git git = new Git(repo);
            // Stage all files in the repo including new files
            //dont be lazy
            //you must filename on this.
            git.add().addFilepattern(".").call();
    
            // and then commit the changes.
            git.commit()
                    .setMessage("Commit all changes including additions")
                    .call();

            try(PrintWriter writer = new PrintWriter(myfile)) {
                writer.append("Hello, world!");
            }

            // Stage all changed files, omitting new files, and commit with one command
            git.commit()
                    .setAll(true)
                    .setMessage("Commit changes to all files")
                    .call();
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoFilepatternException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GitAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private static final int methodOnLogidDesignClass = 0;
//////////////////////////////////////below method is in LogicDesignClass/////////////////////////////////////////

	
	public void mainpageSample(){
		DirectoryUrlDaoClass dao = new DirectoryUrlDaoClass();
		String[] pages = dao.getMainPage();
		for (String page : pages) {
			System.out.println(page);
		}
	}
	
	public void notLoadPage(){
		DirectoryUrlDaoClass dao = new DirectoryUrlDaoClass();
		DirectoryUrlDataClass data = dao.getOneUrlNotLoaded("http://jp.reuters.com");
		System.out.println("" + data.haveLoaded);
	}
	
	public void loadPage(){
		UsingMethodTestClass tes = new UsingMethodTestClass();
		DirectoryUrlDataClass data = new DirectoryUrlDataClass();
		data.directory = "/investing/currencies";
		data.topPage = "http://jp.reuters.com";
		tes.insertUrlsIfUnegisteredFromPage(data);
	}
	
	public void downloadPage(){
		UsingMethodTestClass tes = new UsingMethodTestClass();
		tes.downloadAsHtml("http://jp.reuters.com", "/investing/currencies");
	}
	
	
}
