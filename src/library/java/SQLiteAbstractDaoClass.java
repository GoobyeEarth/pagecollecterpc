package library.java;
/*
 * this check comment of //edit
 * this class is dummy 
 * copy and change
 * DATACLASS to class name
 * check //edit
 * 
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public abstract class SQLiteAbstractDaoClass{
	private Statement stmt;
	private ResultSet rs;
	private Connection con;
	
	//edit 
	private static final String DATABASE_NAME = "database";
	private static final String TABLE_NAME = "table";
	private static final ColumnClass[] COLUMNS = {
			new ColumnClass("symbol", "text"),
			new ColumnClass("date", "integer"),
			new ColumnClass("opening", "real"),
			new ColumnClass("high", "real"),
			new ColumnClass("low", "real"),
			new ColumnClass("closing", "real"),
			new ColumnClass("turnover", "integer"),
			};
	private static final String WORKSPACE = "workspace";
	
	public SQLiteAbstractDaoClass() {

		// JDBCドライバーの指定
		try {
			Class.forName("org.sqlite.JDBC");

			// データベースに接続する なければ作成される
			
			con = DriverManager.getConnection("jdbc:sqlite:" + WORKSPACE
					+ DATABASE_NAME + ".db");
			stmt = con.createStatement();
			
			rs = stmt.executeQuery("select count(*) from sqlite_master where type='table' and name='"
							+ TABLE_NAME + "';");

			// テーブル作成
			if (rs.getInt(1) == 0) {

				String sql = "create table " + TABLE_NAME + "( ";

				sql = sql + COLUMNS[0].name + " " + COLUMNS[0].type;
				for (int i = 1; i < COLUMNS.length; i++) {
					sql = sql + ", " + COLUMNS[i].name + " " + COLUMNS[i].type;
				}
				sql = sql + ")";

				stmt.executeUpdate(sql);

			}
		} catch (ClassNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	public boolean insert(DATACLASS entity) {
		try {
			//edit 
			// String data neads "'" +  + "'"
			String sql = "insert into " + TABLE_NAME + " values ( "
					+ entity.id
					+ ", " + "'" + entity.data + "'"
					
					 + " )";

			return stmt.execute(sql);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return false;
		}

	}

	public void insert(List<DATACLASS> entityList){
		try {
//			String sql = "insert into " + TABLE_NAME + " values (?,?,?,?,?,?,?)";
			String sql = "insert into " + TABLE_NAME + " values (?";
			for(int i = 0; i < COLUMNS.length; i++) {
				sql += ",?";
			}
			sql += ")";
			
			con.setAutoCommit(false);
			PreparedStatement ps = con.prepareStatement(sql);
			for(int i=0; i < entityList.size(); i++){
				DATACLASS entity = entityList.get(i);
				//edit
				ps.setInt(1, entity.id);
				ps.setString(2, entity.data);

				ps.executeUpdate();

			}

			con.commit();

            ps.close();
            con.setAutoCommit(true);



		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}


	}


	public List<DATACLASS> findAll() {
		List<DATACLASS> entityList = new ArrayList<DATACLASS>();
		 PreparedStatement ps;
		// 結果を表示する
		try {
			rs = stmt.executeQuery("select * from " + TABLE_NAME);
			while (rs.next()) {

				entityList.add(toEntity(rs) );

			}
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック

			System.out.println("error");
			e.printStackTrace();
		}

		return entityList;
	}

	private DATACLASS toEntity(ResultSet rs) throws SQLException {

		DATACLASS entity = new DATACLASS();
		//edit
		entity.id = rs.getInt(COLUMNS[0].name);
		entity.data = rs.getString(COLUMNS[1].name);
		



		return entity;

	}

	/**
	 * 編集をしなおしてください
	 * @param selection
	 * @return
	 */
	public List<DATACLASS> findBySymbol(String symbol) {
		List<DATACLASS> entityList = new ArrayList<DATACLASS>();

		// 結果を表示する
		try {

			rs.setFetchSize(10000);

			rs = stmt.executeQuery("select * from " + TABLE_NAME
					+ " where symbol='" + symbol + "'"
					+ " order by date asc");

			while (rs.next()) {
				entityList.add(toEntity(rs) );

			}
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		return entityList;
	}



	public List<String> getSymbolList(){
		List<String> symbolList = new ArrayList<String>();
		try{
			rs = stmt.executeQuery("select distinct symbol from " + TABLE_NAME);
			while(rs.next()){
				symbolList.add(rs.getString("symbol") );
			}
		}catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		return symbolList;
	}

	public List<Integer> getDateList(){
		List<Integer> symbolList = new ArrayList<Integer>();
		try{
			rs = stmt.executeQuery("select distinct date from " + TABLE_NAME
					+ " order by date asc");
			while(rs.next() ){
				symbolList.add(rs.getInt("date") );
			}
		}catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		return symbolList;

	}
	

	public static class ColumnClass{
		public String name;
		public String type;
		
		public ColumnClass(String name, String type){
			this.name = name;
			this.type = type;
		}
	}
	
	public class DATACLASS{
		public int id;
		public String data;
		
	}
	
	////////////////////////////////////////not updated//////////////////////////////
}
